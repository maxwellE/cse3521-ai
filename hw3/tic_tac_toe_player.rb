#!/usr/bin/env ruby
require_relative 'TicTacToeSimulator'
include TicTacToeSimulator
GAME_STATE = [
  [' ',' ',' '],
  [' ', ' ',' '],
  [' ',' ',' ']
]
def main
  player = 'x'
  until (is_winning(GAME_STATE, 'x') || is_winning(GAME_STATE,switch_player(player)) || 
      is_tie(GAME_STATE))
    if player == 'x'
      puts 'Searching now, please wait.'
      x,y = find_best_move(GAME_STATE) 
      GAME_STATE[y][x] = 'x'
    else
      puts "Your move human."
      GAME_STATE | grab_player_input(GAME_STATE,player)
    end
    print_pretty_state(GAME_STATE)
    player = player=='x' ? 'o' : 'x' 
  end
  
  if is_winning(GAME_STATE, 'x')
    puts "I win"
  elsif is_winning(GAME_STATE, switch_player(player))
    puts "You win...this time"
  else
    puts "Tie game"
  end
end

if $0 == __FILE__
  trap("INT") do
    puts "QUIT\nThanks for playing with me!"
    exit
  end
  main
end
