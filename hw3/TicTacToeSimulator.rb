module TicTacToeSimulator
  def minimax(state)
    max_trans = nil
    max_u = nil
    transitions = possible_transitions(state, 'x')
    # Find the transition (move) that provides the maximum
    # utility, assuming the opponent also makes a best move
    transistions.each_pair do |trans, nextstate|
        # after making our move, find the best move the
        # opponent can make (best for opponent = worse for us;
        # if we consider the opponent winning as negative
        # utility, we want to find the minimum utility move
        # of the opponent's possible moves)
        u = min_utility(nextstate, switch_player(player))
        if max_u.nil? or u > max_u
            max_trans = trans
            max_u = u
        end
    end
    max_trans
  end

def min_utility(state, player)
    # if the current state is a win/loss/tie, stop searching
    if is_winning(state, player) ||
            is_winning(state, switch_player(player)) ||
            is_tie(state)
        return utility(state, 'x')
    else
        transitions = possible_transitions(state, switch_player(player))
        min_u = nil
        transitions.each_value do |nextstate|
            # after making a move (current player is in the
            # "player" variable), find the minimum next
            # move and return its utility
            u = max_utility(nextstate, switch_player(player))
            if min_u.nil? or u < min_u
                min_u = u
            end
        end
        return min_u
    end
  end

  def max_utility(state, player)
    # if the current state is a win/loss/tie, stop searching
    if is_winning(state, player) || 
            is_winning(state, switch_player(player)) ||
            is_tie(state)
        return utility(state, 'x')
    else
        transitions = possible_transitions(state, switch_player(player))
        max_u = nil
        transitions.each_value do |nextstate|
            # after making a move (current player is in the
            # "player" variable), find the maximum next
            # move and return its utility
            u = min_utility(nextstate,switch_player(player))
            if max_u.nil? or u > max_u
                max_u = u
            end
        end
        return max_u
    end
  end

  def switch_player(player)
    if player == 'x'
     return 'o'
    else 
     return 'x'
    end
  end

  def is_winning(state, player)
    winning = false
    for i in [0,1,2]
      if state[i][0] == player && state[i][1] == player && state[i][2] == player
        winning = true
      end
    end
    for j in [0,1,2]
      if state[0][j] == player && state[1][j] == player && state[2][j] == player
        winning = true
      end
    end
    if state[0][0] ==player && state[1][1] == player && state[2][2] == player
      winning = true
    end
    if state[0][2] == player && state[1][1] == player && state[2][0] == player
      winning = true
    end
    winning
  end

  def is_tie(state)
    blanks = 0
    for i in [0,1,2]
        for j in [0,1,2]
             blanks += 1 if state[i][j] == ' '
        end
    end
    return blanks == 0 && !is_winning(state, 'x') && !is_winning(state, 'o')
  end

  def utility(state, player)
    return 1 if is_winning(state, player)
    return -1 if is_winning(state, switch_player(player))
    return 0
  end

  def find_best_move(state)
    return nil if is_winning(state, 'x') || is_winning(state,'o') || is_tie(state)
    transistions = possible_transitions(state, 'x')
    max_u = nil
    max_move = nil
    u = nil
    transistions.each_pair do |move, nextstate|
      u = min_utility(nextstate,'x')
      if max_u.nil? || u > max_u
        max_u = u
        max_move = move
      end
    end
      coords = max_move.to_s.split(',').map(&:to_i)
      return coords.first,coords.last
  end

  def possible_transitions(state, player)
    trans_map = {}
    state.each_with_index do |row, y|
      row.each_with_index do |cell,x|
        if cell == ' '
          trans_map["#{x},#{y}".to_sym] = Marshal.load(Marshal.dump(state))
          trans_map["#{x},#{y}".to_sym][y][x] = player
        end 
      end
    end
    trans_map
  end

  def possible_moves_for_player(state)
    moves =[]
    state.each_with_index do |row, y|
      row.each_with_index do |cell,x|
        if cell == ' '
          moves.push "#{x},#{y}".to_sym
        end
      end
    end
    moves
  end

  def grab_player_input(state,player)
    print "Move (X,Y) coordinates?: "
    coords = gets.chomp
    coords =~ /^(\d)\s*,\s*(\d)$/
    if $1.nil?
      xcor = -1
    else
      xcor = $1.to_i
    end
    if $2.nil?
      ycor = -1
    else
      ycor = $2.to_i
    end
    until possible_moves_for_player(state).include? "#{xcor},#{ycor}".to_sym
      puts "Invalid coordinates provided!"
      puts "Acceptable options include:" 
      puts possible_moves_for_player(state)
      print "Move (X,Y) coordinates?: "
      coords = gets.chomp
      coords =~ /^(\d)\s*,\s*(\d)$/
      xcor = $1.to_i
      ycor = $2.to_i
    end
    state[ycor][xcor] = 'o'
    state
  end

  def print_pretty_state(state)
    state.collect{|e|e.join(' | ')}.collect{|x| x.insert(0,' ')}.
      each{|x| puts x; puts '---+---+---'}
  end
end
