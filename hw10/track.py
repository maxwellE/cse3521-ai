
import cv2
import math

backsub = cv2.BackgroundSubtractorMOG()

capture = cv2.VideoCapture("Balcony4_Vis.mpg")
#capture = cv2.VideoCapture(-1)
if capture:
    while True:
        ret, frame = capture.read()
        if ret:
            fgmask = backsub.apply(frame, None, 0.01)
            contours, hierarchy = cv2.findContours(fgmask.copy(), cv2.RETR_EXTERNAL,
                                                   cv2.CHAIN_APPROX_NONE)
            try:
                hierarchy = hierarchy[0]
            except:
                hierarchy = []
            coormap = {}
            best_id = 0
            for contour, hier in zip(contours, hierarchy):
                (x, y, w, h) = cv2.boundingRect(contour)
                if w > 10 and h > 10:
                    set_id = False
                    for cid, pair in coormap.items():
                        coor = pair[0]
                        pair[1] += 1
                        if pair[1] > 8:
                            coormap.pop(cid)
                        else:
                            distance = math.sqrt(math.pow((x - coor[0]), 2) + math.pow((y - coor[1]), 2) + math.pow((w - coor[2]), 2) + math.pow((h - coor[3]), 2))
                            if distance < 50:
                                best_id = cid
                                set_id = True
                                break
                    if not set_id:
                        best_id = len(coormap) + 1
                        coormap[best_id] = [(x, y, w, h), 0]
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
                    cv2.putText(frame, str(best_id), (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX,
                                0.5, (255, 0, 0), 2)
            cv2.imshow("Track", frame)

            key = cv2.waitKey(10)
            if key == ord('q'):
                break
