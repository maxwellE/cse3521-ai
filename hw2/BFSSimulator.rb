#!/usr/bin/env ruby
class BFSSimulator
  NODE_MAP = { 
    "High & Goodale" => ["High & 5th","Goodale parking lot"],
    "High & 5th" => ["High & Goodale", "I-71 5th offramp", "High & King"],
    "I-71 5th offramp" => ["High & 5th", "I-71 11th offramp"],
    "I-71 11th offramp" => ["I-71 17th offramp", "I-71 5th offramp", "High & 11th"],
    "Goodale parking lot" => ["Park & Goodale", "US-23 & Goodale", "High & Goodale"],
    "High & King" => ["SR-315 & King", "High & 5th", "High & 11th"],
    "High & 11th" => ["High & 15th","High & King","I-71 11th offramp"],
    "High & 15th" => ["US-23 & 15th", "High & 11th","High & Woodruff"],
    "Park & Goodale" => ["Goodale parking lot", "Park & Vine"],
    "US-23 & Goodale" => ["Goodale parking lot","US-23 & 15th"],
    "SR-315 & King" => ["High & King","SR-315 & Lane"],
    "Park & Vine" => ["Park & Goodale", "SR-315 I-670 offramp"],
    "US-23 & 15th" => ["High & 15th", "US-23 & 17th","US-23 & Goodale"],
    "I-71 17th offramp" => ["US-23 & 17th","I-71 11th offramp"],
    "SR-315 & Lane" => ["Lane & Tuttle","SR-315 I-670 offramp","SR-315 & King"],
    "Lane & Tuttle"=> ["SR-315 & Lane","Woodruff & Tuttle"],
    "Woodruff & Tuttle" => ["Lane & Tuttle","High & Woodruff"],
    "High & Woodruff"=> ["Woodruff & Tuttle","High & 15th"],
    "US-23 & 17th" => ["US-23 & 15th","I-71 17th offramp"],
    "SR-315 I-670 offramp" => ["Park & Vine", "SR-315 & Lane"]
  }
  attr_accessor :start_state, :end_state
  def initialize
    yield self
  end

  def self.allowed_nodes
    NODE_MAP.keys
  end

  def get_valid_path
    closedset = []
    openset = []
    openset.push @start_state
    parents = Hash.new
    until openset.empty?
      state = openset.shift
      if state == @end_state
        return get_history(parents,state,[])
      else
        closedset.push state
        NODE_MAP[state].each do |next_state|
          next if closedset.include?(next_state)
          openset.push next_state
          parents[next_state] = state
        end
      end
    end
  end

  private

  def get_history(parents,state,result)
    if state == @start_state
      return result.unshift state
    else
      result.unshift state
      return get_history(parents,parents[state],result) 
    end
  end
end

if $0 == __FILE__
  if ARGV.size == 2
    if BFSSimulator.allowed_nodes.include?(ARGV.first) && 
      BFSSimulator.allowed_nodes.include?(ARGV.last)
      bfs = BFSSimulator.new{ |bfs| bfs.start_state = ARGV.first
                    bfs.end_state = ARGV.last}
      puts "A possible route:"
      puts bfs.get_valid_path.collect{|x| "\"#{x}\""}.join("--->")
    else
      puts "\nOne of the provided nodes is invalid!\nAcceptable options include: #{BFSSimulator.
                                                              allowed_nodes.collect{|x| "\"#{x}\""}.join("\n")}"
      exit
    end
  else
    puts "\nInsufficent command line arguments provided!\nCorrect format is: $ ./BFSSimulator \"<start_state>\" \"<end_state>\""
    exit
  end
end
