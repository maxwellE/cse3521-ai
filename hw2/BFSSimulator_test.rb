require_relative 'BFSSimulator'
require 'test/unit'
class TestBFSSimulator < Test::Unit::TestCase
  def test_should_get_short_path
    bfs = BFSSimulator.new{|b| b.start_state = "High & Goodale"
                           b.end_state = "I-71 5th offramp" }
    assert_equal(bfs.get_valid_path, ["High & Goodale", "High & 5th",
                                      "I-71 5th offramp"])
  end

  def test_should_get_medium_path
    bfs = BFSSimulator.new{|b| b.start_state = "High & Goodale"
                           b.end_state = "High & 15th" }
    assert_equal(bfs.get_valid_path, ["High & Goodale", "High & 5th",
                                      "I-71 5th offramp","I-71 11th offramp",
                                      "High & 11th","High & 15th"])
  end

  def test_should_get_short_path_king
    bfs = BFSSimulator.new{|b| b.start_state = "High & Goodale"
                           b.end_state = "High & King"}
    assert_equal(bfs.get_valid_path,["High & Goodale","High & 5th",
                                     "High & King"])

  end
  
  def test_should_not_stack_overflow
    bfs = BFSSimulator.new{|b| b.start_state = "US-23 & Goodale"
                           b.end_state = "SR-315 & King" }
    assert_nothing_thrown do 
      bfs.get_valid_path
    end
  end
end
